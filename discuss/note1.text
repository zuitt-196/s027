What is a Data Model?

	A data model describes how data is organized and grouped in a database.

	By creating data models, we can anticipate which data will be manage by the Database Management System in accordance to the application to be developed.

Data Modelling
	
	Database should have a purpose and its organization must be related to the kind of application we are building.

//Scenario
	A course booking system application where a user can book into a course


Type: Course Booking System
Description: A course booking system application where a user can book into a course

Features:
	-User Registration
	-User Authentication/Login
		Authenticated Users:
			-View Course
			-Enroll Course
			-Update Details (with Admin Verification)
			-Delete Details (with Admin Verification)
		Admin Users:
			-Add Course
			-View/Manage User Accounts
			-Update Course
			-Archive/De-activate Course
			-Re-activate Course
			-View All Courses (active/inactive)
		All Users (guests,authenticated,admin)
			-View Active Courses

Data Models
	-Blueprints for our documents that we can follow and structure our data.
	-Show the relationship/s between our data.

	user {

		id - unique identifier for the document,
		username,
		firstName,
		lastName,
		email,
		password,
		mobileNumber,
		isAdmin

	}

	course {

		id - unique for the document
		name,
		description,
		price,
		slots,
		schedule,
		instructor,
		isActive

	}

	enrollment {

		id - document identifier,
		userId - the unique identifier for the user,
		username - optional,
		courseId - the unique identifier for the course,
		courseName - optional,
		isPaid,
		dateEnrolled

	}


Model Relationships

	To be able to properly organize an application database we should also be able to identify the relationships between our models.

	One to One - This relationship means that a model is exclusively related to only one model.

	Employee:
	{
		"id": "2022Dev",
		"firstName": "Jack",
		"lastName": "Sullivan",
		"email": "jsdev2021@gmail.com"
	}

	Credentials:
	{
		"id": "creds_01",
		"employee_id": "2022dev",
		"role": "developer",
		"team": "tech"
	}

	In MongoDB, one to one relationship can be expressed in another way instead of referencing.

		Embedding - embed/put another document in a document.
			Subdocuments - are documents embedded in a parent document

	Employee
		{
			"id": "2022Dev",
			"firstName": "Jack",
			"lastName": "Sullivan",
			"email": "jsdev2021@gmail.com",
			"credentials": 	{
				"id": "creds_01",
				"role": "developer",
				"team": "tech"
			}
		}

	One to Many

		One model is related to multiple other models.
		However the other models are only related to one.

		Person - many email address

		Email Address - one person

		Blog post - comments

			A blog post can have multiple comments but each comment should only refer to single blog post.

		Blog : {

			"id": "blog1-22",
			"title": "This is an Awesome Blog!",
			"content": "This is an awesome blog that I created!",
			"createdOn": "7/26/2022",
			"author": "blogwriter1"

		}

		Comments: 
		{
			"id":"blogcomment1",
			"comment": "Awesome Blog!",
			"author": "blogwriter1",
			"blog_id": "blog1-22"
		}
		{
			"id":"blogcomment2",
			"comment": "Meh. Not awesome at all.",
			"author": "notHater22",
			"blog_id": "blog1-22"
		}

		In MongoDB, one to many relationship can also be expressed in another way:

			Subdocument Array - an array of subdocuments per single parent document

		Blog : {

			"id": "blog1-22",
			"title": "This is an Awesome Blog!",
			"content": "This is an awesome blog that I created!",
			"createdOn": "7/26/2022",
			"author": "blogwriter1",
			"comments": [

				{
					"id":"blogcomment1",
					"comment": "Awesome Blog!",
					"author": "blogwriter1"
				},
				{
					"id":"blogcomment2",
					"comment": "Meh. Not awesome at all.",
					"author": "notHater22"
				}
			]
		}


		Many to Many

			Multiple documents are related to multiple documents.

			users - courses

			When a many to many relationship is created, for models to relate to each other, an associative entity is created. Associative entity is a model that relates models in the many to many relationship

			user - enrollment - course

			So that a user can relate to a course, so that we can track the enrollment of a user to a course, we have to create the details for their enrollment.

			With Referencing

			user {

				id - unique identifier for the document,
				username,
				firstName,
				lastName,
				email,
				password,
				mobileNumber,
				isAdmin

			}

			course {

				id - unique for the document
				name,
				description,
				price,
				slots,
				schedule,
				instructor,
				isActive

			}

			enrollment {

				id - document identifier,
				userId - the unique identifier for the user,
				username - optional,
				courseId - the unique identifier for the course,
				courseName - optional,
				isPaid,
				dateEnrolled

			}

	In MongoDB, many to many relationship can also be expressed in an another way:

		Two Way Embedding - In two way embedding, the associative entity is created and embedded in both models/documents

		user {

			id - unique identifier for the document,
			username,
			firstName,
			las+-
			email,
			password,
			mobileNumber,
			isAdmin,
			enrollments: [
				{

					id - document identifier,
					courseId - the unique identifier for the course,
					courseName - optional,
					isPaid,
					dateEnrolled
				}
			]

		}


		course {

			id - unique for the document
			name,
			description,
			price,
			slots,
			schedule,
			instructor,
			isActive,
			enrollees: [

				{
					id - document identifier,
					userId,
					userName(optional),
					isPaid,
					dateEnrolled
				}

			]

		}



		sir TJ   -   
						Enrollment
						sirTJ
						Python

						Enrollment
						sirTJ
						React

						Enrollment
						sirTJ
						Java


	Mam Tine			Enrollment                 Java
						mamTine
						Java

	Sir Alvin			Enrollment 				   Java
						sirAlvin
						Java

	Sir Earl			Enrollment                 Java
						sirEarl
						Java

	
	What is the easiest way to identify their relationship?

	1. Many to Many relationship - creates an associative entity to connect our models

	entity 1      1 to many   associative   many to 1     entity2
